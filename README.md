# about.outloudvi

Site: <https://about.outv.im>

Repository: [gitlab:outloudvi/about.outloudvi](https://gitlab.com/outloudvi/about.outloudvi)

Mirror: [github:outloudvi/about.outloudvi](https://github.com/outloudvi/about.outloudvi)

## Components Used
* React
* Semantic UI
* Font Awesome
* LESS
* Webpack