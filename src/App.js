import React from 'react'
import { Button, Grid, Header, List, Segment, Image, Divider } from 'semantic-ui-react'

import { CustomMessage, Navbar } from 'components'
import 'styling/semantic.less'
import './App.less';

import Background from './background.jpg';

var bgStyle = {
  backgroundImage: `url(${Background})`,
  backgroundSize: "cover"
}

const App = () => (
  <Segment className='fullPage'>
    <Grid className='fullPage' style={bgStyle}>
      <Grid.Column className='fullPage gridColumn' computer={8} mobile={16}>
        <div className="lWrapper">
          <Image
            src='https://avatars2.githubusercontent.com/u/19144373'
            className='profileImage'
            size='middle'
          />
          <div
            className='divName divBigName'
          >Outvi V</div>
        </div>
      </Grid.Column>
      <Grid.Column className='fullPage gridColumn' computer={8} mobile={16}>
        <div className="rWrapper">
          <Header as='h1'>
            Outvi V
          <Header.Subheader>a.k.a. outloudvi</Header.Subheader>
          </Header>
          <Divider />
          <List>
            <List.Item>
              <List.Icon name='language' />
              <List.Content> Simplified Chinese (zh_hans), English (en)</List.Content>
            </List.Item>
            <List.Item>
              <List.Icon name='github' />
              <List.Content>
                <a href='https://github.com/outloudvi'>outloudvi</a>
              </List.Content>
            </List.Item>
            <List.Item>
              <List.Icon name='at' />
              <List.Content>
                Mastodon <a href='https://moe.cat/@outvi'>@outvi@moe.cat</a> |
                Twitter <a href='https://twitter.com/@outloudvi'>@outloudvi</a>
              </List.Content>
            </List.Item>
            <List.Item>
              <List.Icon name='telegram' />
              <List.Content>
                <a href='https://t.me/outvi'>@outvi</a>
              </List.Content>
            </List.Item>
            <List.Item>
              <i class="icon faFont fab fa-keybase"></i>
              <List.Content>
                <a href='https://keybase.io/outloudvi'>@outloudvi</a>
              </List.Content>
            </List.Item>
            <List.Item>
              <List.Icon name='key' />
              <List.Content>
                <a href='https://pgp.mit.edu/pks/lookup?op=vindex&search=0xE14A40B70A930B8B'>0xE14A40B70A930B8B</a>
              </List.Content>
            </List.Item>
            <List.Item>
              <List.Icon name='linkify' />
              <List.Content>
                <a href='https://blog.outv.im'>blog.outv.im</a>
              </List.Content>
            </List.Item>
          </List>
          <Divider />
          <div>
            Someone striving to get full-stacked. <br />
            <b><a href='resume_rev4.pdf'>My resume</a></b>
          </div>
        </div>
      </Grid.Column>
    </Grid>
    <div id='bottomFlex'>
      <div className='endingBox'>
        Photo by Jasper van der Meij on <a href="https://unsplash.com/photos/k7D1O46HPwA">Unsplash</a> | Page built w/ <a href="https://semantic-ui.com/">Semantic UI</a> and <a href="https://reactjs.org/">React</a>
      </div>
    </div>
  </Segment>
)

export default App
